Symbiosis
===========================
docker container ls -a
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
//docker rmi $(docker images -q) --force
//docker system prune
//docker volume prune
//docker container prune
//docker system prune ... double check
docker container ls -a

## Write docker-compose.yaml
update folder names in volumes:
      - ./chaincode/items:/opt/gopath/src/items
      - ./chaincode/cates:/opt/gopath/src/cates

## Fabric-CA
0. make clean
term2$ 1. docker-compose -f docker-compose-ca.yaml up
// [INFO] Stored Issuer revocation public key at /etc/hyperledger/fabric-ca/crypto-config/ordererOrganizations/orderer/msp/IssuerRevocationPublicKey
   sudo chmod 777 -R crypto-config
3. make gen_genesis_block
4. make gen_channel_artifacts
5. docker-compose -f docker-compose-ca.yaml down
6. docker-compose up


## SSL Decoder
https://www.sslshopper.com/certificate-decoder.html

## Upgrade to 2.0
https://hyperledger-fabric.readthedocs.io/en/release-2.0/upgrade_to_newest_version.html

## Commands
```sh
docker exec -it cli-peer0-org1 bash -c 'peer channel create -c main -f ./channels/main.tx -o orderer0:7050 --tls --cafile=/etc/hyperledger/orderers/msp/tlscacerts/orderers-ca-7054.pem'

docker exec -it cli-peer0-org1 bash -c 'cp main.block ./channels/'

docker exec -it cli-peer0-org1 bash -c 'peer channel join -b channels/main.block'
docker exec -it cli-peer1-org1 bash -c 'peer channel join -b channels/main.block'
docker exec -it cli-peer0-org2 bash -c 'peer channel join -b channels/main.block'
docker exec -it cli-peer1-org2 bash -c 'peer channel join -b channels/main.block'

docker exec -it cli-peer0-org1 bash -c 'peer channel update -o orderer0:7050 --tls --cafile=/etc/hyperledger/orderers/msp/tlscacerts/orderers-ca-7054.pem -c main -f channels/org1anchor.tx'
docker exec -it cli-peer0-org2 bash -c 'peer channel update -o orderer0:7050 --tls --cafile=/etc/hyperledger/orderers/msp/tlscacerts/orderers-ca-7054.pem -c main -f channels/org2anchor.tx'

sleep 15

cd chaincode/items
go build items.go
go mod vendor
add META_INF/.../couchdb/
  indexId.json, indexName.json, indexWeight.json

cd .../cates
go build xyz.go
go mod vendor
add META_INF/.../couchdb/xyz.json

#------------== Copy chaincode
docker exec -it cli-peer0-org1 bash -c 'peer lifecycle chaincode package items.tar.gz --path /opt/gopath/src/items --lang golang --label items_1'
docker exec -it cli-peer1-org1 bash -c 'peer lifecycle chaincode package items.tar.gz --path /opt/gopath/src/items --lang golang --label items_1'
docker exec -it cli-peer0-org2 bash -c 'peer lifecycle chaincode package items.tar.gz --path /opt/gopath/src/items --lang golang --label items_1'
docker exec -it cli-peer1-org2 bash -c 'peer lifecycle chaincode package items.tar.gz --path /opt/gopath/src/items --lang golang --label items_1'



#------------== Install chaincode
docker exec -it cli-peer0-org1 bash -c 'peer lifecycle chaincode install items.tar.gz &> pkg.txt'
docker exec -it cli-peer1-org1 bash -c 'peer lifecycle chaincode install items.tar.gz'
docker exec -it cli-peer0-org2 bash -c 'peer lifecycle chaincode install items.tar.gz &> pkg.txt'
docker exec -it cli-peer1-org2 bash -c 'peer lifecycle chaincode install items.tar.gz'


#------------== Approve chaincode
# $(tail -n 1 pkg.txt | awk 'NF>1{print $NF}')
docker exec -it cli-peer0-org1 bash -c 'peer lifecycle chaincode approveformyorg -o orderer0:7050 --tls --cafile=/etc/hyperledger/orderers/msp/tlscacerts/orderers-ca-7054.pem --channelID main --name items --version 1.0 --sequence 1 --package-id $(tail -n 1 pkg.txt | awk '\''NF>1{print $NF}'\'')'
docker exec -it cli-peer0-org2 bash -c 'peer lifecycle chaincode approveformyorg -o orderer0:7050 --tls --cafile=/etc/hyperledger/orderers/msp/tlscacerts/orderers-ca-7054.pem --channelID main --name items --version 1.0 --sequence 1 --package-id $(tail -n 1 pkg.txt | awk '\''NF>1{print $NF}'\'')'



#------------== Commit chaincode
# I believe you only need to do this for ONE of the peers in the network
docker exec -it cli-peer0-org1 bash -c 'peer lifecycle chaincode commit -o orderer0:7050 --tls --cafile=/etc/hyperledger/orderers/msp/tlscacerts/orderers-ca-7054.pem --channelID main --name items --version 1.0 --sequence 1'

# docker exec -it cli-peer0-org2 bash -c 'peer lifecycle chaincode commit -o orderer0:7050 --tls --cafile=/etc/hyperledger/orderers/msp/tlscacerts/orderers-ca-7054.pem --channelID main --name items --version 1.0 --sequence 1'



#--------------== Write to the chaincode
docker exec -it cli-peer0-org1 bash -c 'peer chaincode invoke -C main -n items -c '\''{"Args":["Create", "1","{\"id\":\"1\",\"name\":\"Iron Ore\",\"cate_id\":\"1\",\"weight\":4000,\"visible\":true}"]}'\'' -o orderer0:7050 --tls --cafile=/etc/hyperledger/orderers/msp/tlscacerts/orderers-ca-7054.pem'

docker exec -it cli-peer0-org1 bash -c 'peer chaincode invoke -C main -n items -c '\''{"Args":["Create", "2","{\"id\":\"2\",\"name\":\"Copper Ore\",\"cate_id\":\"2\",\"weight\":9000,\"visible\":false}"]}'\'' -o orderer0:7050 --tls --cafile=/etc/hyperledger/orderers/msp/tlscacerts/orderers-ca-7054.pem'

docker exec -it cli-peer0-org1 bash -c 'peer chaincode invoke -C main -n items -c '\''{"Args":["Create", "3","{\"id\":\"3\",\"name\":\"Platinum Ore\",\"cate_id\":\"2\",\"weight\":5000,\"visible\":true}"]}'\'' -o orderer0:7050 --tls --cafile=/etc/hyperledger/orderers/msp/tlscacerts/orderers-ca-7054.pem'



#--------------== Query ALL from the chaincode
docker exec -it cli-peer0-org1 bash -c 'peer chaincode query -C main -n items -c '\''{"Args":["QueryAll"]}'\'' -o orderer0:7050 --tls --cafile=/etc/hyperledger/orderers/msp/tlscacerts/orderers-ca-7054.pem'
docker exec -it cli-peer1-org1 bash -c 'peer chaincode query -C main -n items -c '\''{"Args":["QueryAll"]}'\'' -o orderer0:7050 --tls --cafile=/etc/hyperledger/orderers/msp/tlscacerts/orderers-ca-7054.pem'
docker exec -it cli-peer0-org2 bash -c 'peer chaincode query -C main -n items -c '\''{"Args":["QueryAll"]}'\'' -o orderer0:7050 --tls --cafile=/etc/hyperledger/orderers/msp/tlscacerts/orderers-ca-7054.pem'
docker exec -it cli-peer1-org2 bash -c 'peer chaincode query -C main -n items -c '\''{"Args":["QueryAll"]}'\'' -o orderer0:7050 --tls --cafile=/etc/hyperledger/orderers/msp/tlscacerts/orderers-ca-7054.pem'

#--------------== Read the entry
docker exec -it cli-peer0-org1 bash -c 'peer chaincode invoke -C main -n items -c '\''{"Args":["Read", "2"]}'\'' -o orderer0:7050 --tls --cafile=/etc/hyperledger/orderers/msp/tlscacerts/orderers-ca-7054.pem'

docker exec -it cli-peer0-org1 bash -c 'peer chaincode invoke -C main -n items -c '\''{"Args":["Read", "1"]}'\'' -o orderer0:7050 --tls --cafile=/etc/hyperledger/orderers/msp/tlscacerts/orderers-ca-7054.pem'

#--------------== Update the entry
docker exec -it cli-peer0-org1 bash -c 'peer chaincode invoke -C main -n items -c '\''{"Args":["Update", "1","{\"id\":\"1B\",\"name\":\"IronB\",\"cate_id\":\"cate1B\",\"weight\":4555,\"visible\":false}"]}'\'' -o orderer0:7050 --tls --cafile=/etc/hyperledger/orderers/msp/tlscacerts/orderers-ca-7054.pem'


#--------------== Read the entry
docker exec -it cli-peer0-org1 bash -c 'peer chaincode invoke -C main -n items -c '\''{"Args":["Read", "1"]}'\'' -o orderer0:7050 --tls --cafile=/etc/hyperledger/orderers/msp/tlscacerts/orderers-ca-7054.pem'

#--------------== Rich Query QueryX1
docker exec -it cli-peer0-org1 bash -c 'peer chaincode query -C main -n items -c '\''{"Args":["QueryAll"]}'\'' -o orderer0:7050 --tls --cafile=/etc/hyperledger/orderers/msp/tlscacerts/orderers-ca-7054.pem'

docker exec -it cli-peer0-org1 bash -c 'peer chaincode invoke -C main -n items -c '\''{"Args":["QueryX1", "{\"selector\":{ \"weight\": { \"$gt\":5000 } }}", "0", ""]}'\'' -o orderer0:7050 --tls --cafile=/etc/hyperledger/orderers/msp/tlscacerts/orderers-ca-7054.pem'

docker exec -it cli-peer0-org1 bash -c 'peer chaincode invoke -C main -n items -c '\''{"Args":["QueryX1", "{\"selector\":{ \"weight\": { \"$lt\":5000 } }}", "0", ""]}'\'' -o orderer0:7050 --tls --cafile=/etc/hyperledger/orderers/msp/tlscacerts/orderers-ca-7054.pem'

docker exec -it cli-peer0-org1 bash -c 'peer chaincode invoke -C main -n items -c '\''{"Args":["QueryX1", "{\"selector\":{ \"weight\": { \"$eq\":5000 } }}", "0", ""]}'\'' -o orderer0:7050 --tls --cafile=/etc/hyperledger/orderers/msp/tlscacerts/orderers-ca-7054.pem'


#--------------== QueryByString
docker exec -it cli-peer0-org1 bash -c 'peer chaincode invoke -C main -n items -c '\''{"Args":["QueryByString", "{\"selector\":{ \"weight\": { \"$eq\":5000 } }}"]}'\'' -o orderer0:7050 --tls --cafile=/etc/hyperledger/orderers/msp/tlscacerts/orderers-ca-7054.pem'

docker exec -it cli-peer0-org1 bash -c 'peer chaincode invoke -C main -n items -c '\''{"Args":["QueryByString", "{\"selector\":{ \"cate_id\": { \"$eq\": \"2\"} }}"]}'\'' -o orderer0:7050 --tls --cafile=/etc/hyperledger/orderers/msp/tlscacerts/orderers-ca-7054.pem'

#--------------== QueryByRange
docker exec -it cli-peer0-org1 bash -c 'peer chaincode invoke -C main -n items -c '\''{"Args":["QueryByRange", "1","2"]}'\'' -o orderer0:7050 --tls --cafile=/etc/hyperledger/orderers/msp/tlscacerts/orderers-ca-7054.pem'

docker exec -it cli-peer0-org1 bash -c 'peer chaincode invoke -C main -n items -c '\''{"Args":["QueryByRange", "1","3"]}'\'' -o orderer0:7050 --tls --cafile=/etc/hyperledger/orderers/msp/tlscacerts/orderers-ca-7054.pem'

docker exec -it cli-peer0-org1 bash -c 'peer chaincode invoke -C main -n items -c '\''{"Args":["QueryByRange", "1","4"]}'\'' -o orderer0:7050 --tls --cafile=/etc/hyperledger/orderers/msp/tlscacerts/orderers-ca-7054.pem'

docker exec -it cli-peer0-org1 bash -c 'peer chaincode invoke -C main -n items -c '\''{"Args":["QueryByRange", "2","4"]}'\'' -o orderer0:7050 --tls --cafile=/etc/hyperledger/orderers/msp/tlscacerts/orderers-ca-7054.pem'

#--------------== Delete the entry
docker exec -it cli-peer0-org1 bash -c 'peer chaincode invoke -C main -n items -c '\''{"Args":["Delete", "1"]}'\'' -o orderer0:7050 --tls --cafile=/etc/hyperledger/orderers/msp/tlscacerts/orderers-ca-7054.pem'


#--------------== Backend
#/mnt/sda5/0Programming/HyperLedger/NickKotenberg_modified/s5-connecting-everythingWk/back-end-working-v2/dockerHL211/crypto-config/ordererOrganizations/orderer/msp/tlscacerts/orderers-ca-7054.pem


sleep 15

#change the label name for packaging a new version of the chaincode:
#--label items_2
docker exec -it cli-peer0-org1 bash -c 'peer lifecycle chaincode package items.tar.gz --path /opt/gopath/src/items --lang golang --label items_2'
docker exec -it cli-peer1-org1 bash -c 'peer lifecycle chaincode package items.tar.gz --path /opt/gopath/src/items --lang golang --label items_2'
docker exec -it cli-peer0-org2 bash -c 'peer lifecycle chaincode package items.tar.gz --path /opt/gopath/src/items --lang golang --label items_2'
docker exec -it cli-peer1-org2 bash -c 'peer lifecycle chaincode package items.tar.gz --path /opt/gopath/src/items --lang golang --label items_2'


docker exec -it cli-peer0-org1 bash -c 'peer lifecycle chaincode install items.tar.gz &> pkg.txt'
docker exec -it cli-peer1-org1 bash -c 'peer lifecycle chaincode install items.tar.gz'
docker exec -it cli-peer0-org2 bash -c 'peer lifecycle chaincode install items.tar.gz &> pkg.txt'
docker exec -it cli-peer1-org2 bash -c 'peer lifecycle chaincode install items.tar.gz'


docker exec -it cli-peer0-org1 bash -c 'peer lifecycle chaincode approveformyorg -o orderer0:7050 --tls --cafile=/etc/hyperledger/orderers/msp/tlscacerts/orderers-ca-7054.pem --channelID main --name items --version 2.0 --sequence 2 --package-id $(tail -n 1 pkg.txt | awk '\''NF>1{print $NF}'\'')'
#docker exec -it cli-peer0-org2 bash -c 'peer lifecycle chaincode approveformyorg -o orderer0:7050 --tls --cafile=/etc/hyperledger/orderers/msp/tlscacerts/orderers-ca-7054.pem --channelID main --name items --version 2.0 --sequence 2 --package-id $(tail -n 1 pkg.txt | awk '\''NF>1{print $NF}'\'')'


docker exec -it cli-peer0-org1 bash -c 'peer lifecycle chaincode commit -o orderer0:7050 --tls --cafile=/etc/hyperledger/orderers/msp/tlscacerts/orderers-ca-7054.pem --channelID main --name items --version 2.0 --sequence 2'

```
