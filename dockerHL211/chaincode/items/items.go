package main

import (
	"bytes"
	"errors"
	"fmt"

	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)// todo: import time package into vendor list

func main() {
	itemContract := new(ItemsContract)
	cc, err := contractapi.NewChaincode(itemContract)

	if err != nil {
		panic(err.Error())
	}

	if err := cc.Start(); err != nil {
		panic(err.Error())
	}
}

// ItemsContract contract for handling writing and reading from the world state
type ItemsContract struct {
	contractapi.Contract
}

// Item item
type Item struct {
	ID            string `json:"id"`
	Name          string `json:"name"`
	CateID				string `json:"cate_id"`
	Weight      	float32 `json:"weight"` // in lbs'
	// ArrivalTime *time.Time `json:"arrival_time"`
	// Timestamp   *time.Time `json:"timestamp"`
	Visible     	bool  `json:"visible"`
}

// InitLedger adds a base set of products to the ledger
func (rc *ItemsContract) InitLedger(ctx contractapi.TransactionContextInterface) error {
	return nil
}


//add weight for queryString to try it
//add query string: start and end
//why queryString has dramatic change!!!

//stub shim.ChaincodeStubInterface -> ctx contractapi.TransactionContextInterface
//stub -> ctx.GetStub()

// QueryByRange ... add blocks
func (rc *ItemsContract) QueryByRange(ctx contractapi.TransactionContextInterface, start string, end string) (ret string, err error) {
	if len(start) == 0 || len(end) == 0 {
		return "error", errors.New("start or/and end arguments are missing")
	}
	resultsIterator, err := ctx.GetStub().GetStateByRange(start, end)
	if err != nil {
		return
	}
	defer resultsIterator.Close()

	buffer, err := constructQueryResponseFromIterator(resultsIterator)
	if err != nil {
		return
	}

	return buffer.String(), nil
}

// QueryByString ... add blocks
func (rc *ItemsContract) QueryByString(ctx contractapi.TransactionContextInterface, queryString string) (ret string, err error) {
	if (len(queryString) == 0){
		queryString = `{"selector": {"id":{"$ne":"-"}}}`
	}
	resultsIterator, err := ctx.GetStub().GetQueryResult(queryString)
	if err != nil {
		return "error1", err
	}
	defer resultsIterator.Close()

	buffer, err := constructQueryResponseFromIterator(resultsIterator)
	if err != nil {
		return
	}

	return buffer.String(), nil
}

// QueryX1 ... add blocks, @40:39
func (rc *ItemsContract) QueryX1(
	ctx contractapi.TransactionContextInterface, query string, pageSize int32, bookmark string,
) (ret string, err error) {
	if (len(query) == 0){
		query= `{"selector": {"id":{"$ne":"-"}}}`
	}

	resultsIterator, _, err := ctx.GetStub().GetQueryResultWithPagination(query, pageSize, bookmark)
	if err != nil {
		return
	}
	defer resultsIterator.Close()

	buffer, err := constructQueryResponseFromIterator(resultsIterator)
	if err != nil {
		return
	}

	return buffer.String(), nil
}


// QueryAll - do not add blocks!
func (rc *ItemsContract) QueryAll(
	ctx contractapi.TransactionContextInterface,
) (ret string, err error) {

	resultsIterator, _, err := ctx.GetStub().GetQueryResultWithPagination(`{"selector": {"id":{"$ne":"-"}}}`, 0, "")
	if err != nil {
		return
	}
	defer resultsIterator.Close()

	buffer, err := constructQueryResponseFromIterator(resultsIterator)
	if err != nil {
		return
	}

	return buffer.String(), nil
}

// ===========================================================================================
// constructQueryResponseFromIterator constructs a JSON array containing query results from
// a given result iterator
// ===========================================================================================
func constructQueryResponseFromIterator(resultsIterator shim.StateQueryIteratorInterface) (*bytes.Buffer, error) {
	// buffer is a JSON array containing QueryResults
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return nil, err
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Record\":")
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	return &buffer, nil
}

// Create adds a new key with value to the world state
func (rc *ItemsContract) Create(ctx contractapi.TransactionContextInterface, key string, value string) error {
	if len(key) == 0 || len(value) == 0 {
		return errors.New("key or/and value arguments are missing")
	}
	existing, err := ctx.GetStub().GetState(key)

	if err != nil {
		return errors.New("Unable to interact with world state")
	}

	if existing != nil {
		return fmt.Errorf("Cannot create world state pair with key %s. Already exists", key)
	}

	/*
type Item struct {
	ID             string `json:"id"`
	Name           string `json:"name"`
	CateID string `json:"cate_id"`
	Weight      	float32 `json:"weight"` // in lbs'
  Visible     		bool  `json:"visible"`
}
// ArrivalTime *time.Time `json:"arrival_time"`
// Timestamp   *time.Time `json:"timestamp"`

	var item Item
	if err := json.Unmarshal([]byte(value), &item); err != nil {
		return errors.New(err.Error())
	}

	if len(item.ID) == 0 {
		return errors.New("ID is required to store raw item")
	}
	if item.Weight == 0 {
		return errors.New("Weight is required to store raw item")
	}
	if len(item.Name) == 0 {
		return errors.New("Name is required to store raw item")
	}

	t := time.Now()
	item.Timestamp = &t
	item.Visible = true

	itemAsBytes, err := json.Marshal(item)
	if err != nil {
		return errors.New(err.Error())
	}

	stub.PutState(item.ID, itemAsBytes)
	*/
	err = ctx.GetStub().PutState(key, []byte(value))

	if err != nil {
		return errors.New("Unable to interact with world state")
	}

	return nil
}

// Delete an entry
func (rc *ItemsContract) Delete(ctx contractapi.TransactionContextInterface, key string) error {
	if len(key) == 0 {
		return errors.New("key argument is missing")
	}
	existing, err := ctx.GetStub().GetState(key)

	if err != nil {
		return errors.New("Unable to interact with world state")
	}

	if existing == nil {
		return fmt.Errorf("existing state pair is null with key %s", key)
	}

	err = ctx.GetStub().DelState(key)

	if err != nil {
		return errors.New("Unable to delete the entry key")
	}
	
	return nil;
}


// Update changes the value with key in the world state
func (rc *ItemsContract) Update(ctx contractapi.TransactionContextInterface, key string, value string) error {
	if len(key) == 0 || len(value) == 0 {
		return errors.New("key or/and value arguments are missing")
	}

	existing, err := ctx.GetStub().GetState(key)

	if err != nil {
		return errors.New("Unable to interact with world state")
	}

	if existing == nil {
		return fmt.Errorf("Cannot update world state pair with key %s. Does not exist", key)
	}

	err = ctx.GetStub().PutState(key, []byte(value))

	if err != nil {
		return errors.New("Unable to interact with world state")
	}

	return nil
}

// Read returns the value at key in the world state
func (rc *ItemsContract) Read(ctx contractapi.TransactionContextInterface, key string) (string, error) {
	if len(key) == 0 {
		return "error", errors.New("key argument is missing")
	}

	existing, err := ctx.GetStub().GetState(key)

	if err != nil {
		return "error", errors.New("Unable to interact with world state")
	}

	if existing == nil {
		return "error", fmt.Errorf("Cannot read world state pair with key %s. Does not exist", key)
	}

	return string(existing), nil
}