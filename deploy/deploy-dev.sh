#!/bin/bash
# run this script to upload the new docker image to the Docker Hub

docker build -t cryptoman7701/hyperledger-k8s-be:latest-dev -f Dockerfile .

docker push cryptoman7701/hyperledger-k8s-be:latest-dev

# chmod +x ./deploy/deploy.sh
# docker login
# backend dir $ ./deploy/deploy.sh

# if "cannot connect to the Docker daemon. Is the docker daemon running? dial unix: /var/run/docker.sock.": 
# sudo chmod 777 /var/run/docker.sock
# sudo chown $USER:$USER /var/run/docker.sock
# sudo service docker restart
# docker run hello-world