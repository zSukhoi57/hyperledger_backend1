FROM golang:1.14.4 AS builder

WORKDIR /app

COPY . .
RUN go build -o /app/api main.go

#2nd container
FROM alpine:edge
WORKDIR /app

RUN apk add --no-cache libc6-compat
#later on we will need this for hyperledger to build

COPY --from=builder /app/api /app/api
COPY ./static /app/static

CMD ["/app/api"]
# $ docker build .
# ... returns container id
# $ docker run <returned container id>
# $ docker run -p 3000:3000 <container id>