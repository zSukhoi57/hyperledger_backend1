package main

import (
	"hyperledger-k8s-be/server"
)//will import the interface

func main() {
	s := server.NewServer()
	if err := s.Init(3000); err != nil {
		panic(err)
	}
	s.Start()
}// ./server/server.go