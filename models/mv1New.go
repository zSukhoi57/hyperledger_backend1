package models

import (
	//"time"
	"log"
)

//------------------------------==

//Users ...
type Users []User

//User ...
type User struct {
	ID        string `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Email     string `json:"email"`
	Password  string `json:"password"`
}

//NewUser ...
func NewUser(firstName string, lastName string, email string, password string) (user *User, err error) {
	log.Printf("mv1New > NewUser()")
	user = new(User)
	
	id, err := genUUID() // to generate an unique id at utils.go/genU UID()
	if err != nil {
		return
	}

	user.ID = id
	user.FirstName = firstName
	user.LastName = lastName
	user.Email = email
	if user.Password, err = EncryptPassword(password); err != nil {
		return
	}
	log.Println("mv1New > NewUser():", user.ID, user.FirstName, user.LastName, user.Email)
	return
}

//------------------------------==

//Cates ...
type Cates []Cate

//Cate ...
type Cate struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

//NewCate ...
func NewCate(name string) (cate *Cate, err error) {
	cate = new(Cate)
	if cate.ID, err = genUUID(); err != nil {
		return
	}

	cate.Name = name
	log.Println("mv1New > NewCate():", cate)
	return
}

//------------------------------==

//Items ...
type Items []Item

//Item ...
type Item struct {
	ID          string     `json:"id"`
	Name        string     `json:"name"`
	CateID      string     `json:"cate_id"`
	Weight      float32    `json:"weight"`
	// ArrivalTime *time.Time `json:"arrival_time"`
	// Timestamp   *time.Time `json:"timestamp"`
	Visible			bool				`json:"visible"`
}

//NewItem ...
func NewItem(name string, cateID string, weight float32) (item *Item, err error) {
	//, arrivalTime *time.Time
	item = new(Item)
	if item.ID, err = genUUID(); err != nil {
		return
	}

	item.Name = name
	item.CateID = cateID
	item.Weight = weight

	// t := time.Now()
	// if arrivalTime == nil {
	// 	item.ArrivalTime = arrivalTime
	// } else {
	// 	item.ArrivalTime = &t
	// }
	// item.Timestamp = &t
	log.Println("mv1New > NewItem():", item)
	return
}

