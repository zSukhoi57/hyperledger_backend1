package models

import (
	//"log"
	"github.com/google/uuid"
)//uuid is to generate unique IDs

// cannot use incremental id bcos in blockchain, you don't know what other peers are doing
func genUUID() (ID string, err error) {
	id, err := uuid.NewUUID()
	if err != nil {
		return
	}

	ID = id.String()
	//log.Println("--------==models/v1/util: new ID:", ID)
	return
}
