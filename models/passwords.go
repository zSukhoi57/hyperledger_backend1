package models

import (
	"log"
	"golang.org/x/crypto/bcrypt"
)

const ( 
	salt = 10
)

//IsPasswordValid ...
func IsPasswordValid(hash string, password string) bool {
	log.Printf("modelsV1 > IsPasswordValid()")
	if bcrypt.CompareHashAndPassword(
		[]byte(hash), []byte(password)) != nil {
		return false
	}
	return true
}

//EncryptPassword ...
func EncryptPassword(password string) (string, error) {
	log.Printf("modelsV1 > EncryptPassword()")
	encryptedPwByte, err := bcrypt.GenerateFromPassword(
		[]byte(password), salt)
	return string(encryptedPwByte), err
}