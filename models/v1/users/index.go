package users

import (
	"log"
	"hyperledger-k8s-be/models"
)

//Index ...
func Index() (users *models.Users, err error){
	log.Printf("models_v1_users/index > Index() ...")
	users = &mockUsers

	return
}