package users

import (
	"errors"
	"hyperledger-k8s-be/models"
	"log"
	"github.com/davecgh/go-spew/spew"
)

//assume in the stub code for updates, the user exists on the list of mock data

//UpdateOpts ...
type UpdateOpts struct {
	Replace bool
}

//Update ...
func Update(id string, usr *models.User, opts *UpdateOpts) (*models.User, error) {
	log.Println("--------==models_v1_users/update")
	var exists bool

	log.Println("opts:", opts)
	if opts == nil {
		opts = &UpdateOpts{}
	}
	log.Println("opts:", opts, ", opts.Replace:", opts.Replace)

	for index, user := range mockUsers {
		if user.ID == id {
			if opts.Replace {
				log.Println("--------==models_v1_users/update: opts = replace")
				usr.ID = mockUsers[index].ID
				usr.Password = mockUsers[index].Password
				usr.Email = mockUsers[index].Email
				mockUsers[index] = *usr

			} else {
				log.Println("--------==models_v1_users/update: opts = update")
				if len(usr.FirstName) > 0 {
					mockUsers[index].FirstName = usr.FirstName
				}
				if len(usr.LastName) > 0 {
					mockUsers[index].LastName = usr.LastName
				}
			}

			exists = true
			usr = &mockUsers[index]
		}
	}

	if !exists {
		return nil, errors.New("unable to update user because user was not found")
	}
	log.Println("updated value...")
	spew.Dump(*usr)

	return usr, nil
}
