package users

import (
	"log"
	"hyperledger-k8s-be/models"
	"github.com/davecgh/go-spew/spew"
)
//Store ...
func Store(firstName string, lastName string, email string, password string) (user *models.User, err error) {
	log.Println("--------==models_v1_users/store")
	user, err = models.NewUser(firstName, lastName, email, password)

	if err != nil {
		return
	}
	spew.Dump(*user)
	mockUsers = append(mockUsers, *user)
	return
}