package users

import (
	"errors"
	"log"
)

//Destroy ...
func Destroy(id string) (error) {
	log.Println("--------==models_v1_users/destroy ... id:", id)
	var exists bool

	for index, user := range mockUsers {
		if user.ID == id {
			//very inefficiant... DO NOT DO THIS in production!!
			mockUsers = append(mockUsers[:index], mockUsers[index+1:]...)
			exists = true
		}
	}

	if !exists {
		return errors.New("unable to delete user because user was not found")
	}

	return nil
}