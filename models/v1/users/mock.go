package users

import (
	"log"
	"hyperledger-k8s-be/models"
)

var mockUsers models.Users

func init() {
	log.Printf("models_v1_users/mock > init()")
	usr, _ := models.NewUser("Chris", "Evans", "chris.evans@gmail.com", "1234")

	mockUsers = models.Users{
		*usr, 
	}
}