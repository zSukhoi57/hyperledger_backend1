package items

import (
	"errors"
	"encoding/json"
	"hyperledger-k8s-be/models"
	"log"
	"github.com/davecgh/go-spew/spew"
	"hyperledger-k8s-be/hyperledger"
)

//assume in the stub code for updates, the item exists on the list of mock data

//UpdateOpts ... DO NOT REPLACE THIS TYPE!!!
type UpdateOpts struct {
	Replace bool
}

//Update ... 
func Update(clients *hyperledger.Clients, id string, itm *models.Item, opts *UpdateOpts, isOnlyAPI bool) (*models.Item, error) {
	log.Println("--------==models_v1_items/update")
	var exists bool

	log.Println("opts:", opts)
	if opts == nil {
		opts = &UpdateOpts{}
	}
	log.Println("opts:", opts, ", opts.Replace:", opts.Replace)
	log.Println("input itm:")
	spew.Dump(*itm)

	if opts.Replace {
		log.Println("opts: replace")
		if isOnlyAPI {
			for index, item := range mockItems {
				if item.ID == id {
					log.Println("--------==models/v1/items/update: opts = replace")
					itm.ID = mockItems[index].ID
					mockItems[index] = *itm

					exists = true
				}
			}
			if !exists {
				return nil, errors.New("unable to update item because item was not found")
			}
			log.Println("--------==updated value...")
			spew.Dump(*itm)
			return itm, nil
		}

		//---------------== Use HyperLedger
		packet, err := json.Marshal(itm)
		if err != nil {
			return nil, err
		}
			// stub.PutState(args[0], []byte(args[1]))
		_, err = clients.Invoke("org1", "items", "update", [][]byte{
			[]byte(id),
			packet,
		})//add ',' at the end of arg!
		if err != nil {
			return nil, err
		}

	} else {
		log.Println("opts: update")
		if isOnlyAPI {
			for index, item := range mockItems {
				if item.ID == id {
					log.Println("--------==models/v1/items/update: opts: update")
					log.Println("found item:")
					spew.Dump(item)
				
					if len(itm.Name) > 0 {
						mockItems[index].Name = itm.Name
					}
					if len(itm.CateID) > 0 {
						mockItems[index].CateID = itm.CateID
					}
					if itm.Weight > 0 {
						mockItems[index].Weight = itm.Weight
					}

					exists = true
					itm = &mockItems[index]
				}
			}
			if !exists {
				return nil, errors.New("unable to update item because item was not found")
			}
			log.Println("--------==updated value...")
			spew.Dump(*itm)
			return itm, nil
		}

		//Use HyperLedger

	}

	return itm, nil
}
