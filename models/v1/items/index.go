package items

import (
	"encoding/json"
	"hyperledger-k8s-be/hyperledger"
	"hyperledger-k8s-be/models"
	"log"
)


//Index ...
func Index(clients *hyperledger.Clients, isOnlyAPI bool) (items *models.Items, err error){
	log.Println("--------==models_v1_items/index")
	if isOnlyAPI {
		items = &mockItems
		return
	}

	//---------------== Use HyperLedger
	items = new(models.Items)
	res, err := clients.Query("org1", "items", "queryString", [][]byte{
		[]byte("{\"selector\":{ \"visible\": { \"$eq\":true } }}"),
	})//do not use index() as we need to check for visible value

	// res, err := clients.Query("org1", "items", "index", [][]byte{
	// 	[]byte(""),
	// 	[]byte(""),
	// })

	if err != nil {
		//log.Println("err@query:", err)
		return
	}
	if err = json.Unmarshal(res, items); err != nil {
		//log.Println("err@unmarshal:", err)
		return
	}
	return
}
/*
curl localhost:3000/v1/items
*/