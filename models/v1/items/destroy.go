package items

import (
	//"errors"
	"errors"
	"hyperledger-k8s-be/hyperledger"
	"log"
)

//Destroy ...
func Destroy(clients *hyperledger.Clients, id string, isOnlyAPI bool) (err error) {
	log.Println("--------==models_v1_items/destroy ... id:", id)
	if isOnlyAPI {
		var exists bool

		for index, item := range mockItems {
			if item.ID == id {
				mockItems = append(mockItems[:index], mockItems[index+1:]...)
				exists = true
			}
		}

		if !exists {
			return errors.New("unable to delete item because item was not found")
		}
		return
	}

	//---------------== Use HyperLedger
	res, err := Show(clients, id, isOnlyAPI)
	if err != nil {
		return err
	}
	res.Visible = false

	// stub.PutState(args[0], []byte(args[1]))
	_, err = Update(clients, id, res, &UpdateOpts{ Replace: true}, isOnlyAPI)
	if err != nil {
		return err
	}

	/*
curl localhost:3000/v1/items/<id>
... copy the returned {}, change values 

curl -X DELETE localhost:3000/v1/items/<id from GET call>

curl -X DELETE localhost:3000/v1/items/5d8d342c-f2f1-45d3-87bf-4b130a2d89ef


curl localhost:3000/v1/items/<id>
*/
	return nil
}