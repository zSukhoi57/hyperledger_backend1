package items

import (
	"encoding/json"
	"log"
	//"time"

	"hyperledger-k8s-be/hyperledger"
	"hyperledger-k8s-be/models"

	"github.com/davecgh/go-spew/spew"
	"github.com/satori/go.uuid"
)

//Store ...
func Store(clients *hyperledger.Clients, mineralName string, cateID string, weight float32, isOnlyAPI bool) (item *models.Item, err error) {
	//arrivalTime *time.Time, 
	log.Println("--------==models_v1_items/store")
	item = new(models.Item)
	item.ID = uuid.NewV4().String()
	item.Name = mineralName
	item.CateID = cateID
	item.Weight = weight
	item.Visible = true

	// if arrivalTime != nil {
	// 	item.ArrivalTime = arrivalTime
	// }
	packet, err := json.Marshal(item)
	if err != nil {
		return
	}

	if isOnlyAPI {
		item, err = models.NewItem(mineralName, cateID, weight)//, arrivalTime

		if err != nil {
			return
		}
		log.Println("--------==new items:")
		spew.Dump(*item)
		mockItems = append(mockItems, *item)
		return
	}

	//---------------== Use HyperLedger
	_, err = clients.Invoke("org1", "items", "store", [][]byte{
		packet,
	})//add ',' at the end of arg!
	if err != nil {
		return
	}

	return
}
