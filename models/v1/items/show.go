package items

import (
	"log"
	"encoding/json"
	"errors"
	"hyperledger-k8s-be/models"
	"hyperledger-k8s-be/hyperledger"
)

//assume in the stub code for updates, the item exists on the list of mock data


//Show ...
func Show(clients *hyperledger.Clients, id string, isOnlyAPI bool) (item *models.Item, err error) {
	log.Println("--------==models_v1_items/show")

	if isOnlyAPI {
		for index, item := range mockItems {
			if item.ID == id {
				return &mockItems[index], nil
			}
		}
		return nil, errors.New("unable to find item")
	}


	//---------------== Use HyperLedger
	items := new(models.Items)

	res, err := clients.Query("org1", "items", "queryString", [][]byte{
		[]byte("{\"selector\":{ \"id\": { \"$eq\":\""+id+"\" } }}"),
	})	// "{\"selector\":{ \"weight\": { \"$gt\":5000 } }}"

	if err != nil {
		return
	}
	if err = json.Unmarshal(res, items); err != nil {
		return
	}
	list := *items
	if len(list) == 0 {
		err = errors.New("unable to find item with id "+id)
		return
	}

	item = &list[0]

	return //nil, nil
}
/*
curl localhost:3000/v1/items
... copy the id you want, paste in url below

curl localhost:3000/v1/items/<id>
curl localhost:3000/v1/items/5d8d342c-f2f1-45d3-87bf-4b130a2d89ef
*/