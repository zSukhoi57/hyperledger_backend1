package cates

import (
	"errors"
	"hyperledger-k8s-be/models"
	"log"
	"github.com/davecgh/go-spew/spew"
)

//assume in the stub code for updates, the cate exists on the list of mock data

//UpdateOpts ...
type UpdateOpts struct {
	Replace bool
}

//Update ...
func Update(id string, rstInput *models.Cate, opts *UpdateOpts) (*models.Cate, error) {
	log.Println("--------==models_v1_cates/update()")
	var exists bool

	log.Println(opts)

	if opts == nil {
		opts = &UpdateOpts{}
	}

	for index, cate := range mockCates {
		if cate.ID == id {
			if opts.Replace {
				log.Println("opts = replace")
				rstInput.ID = mockCates[index].ID

			} else {
				log.Println("opts = update")
				if len(rstInput.Name) > 0 {
					mockCates[index].Name = rstInput.Name
				}
			}
			exists = true

			//return the actual new item back
			rstInput = &mockCates[index]
		}
	}

	if !exists {
		return nil, errors.New("unable to update cate because cate was not found")
	}
	log.Println("update finished...")
	spew.Dump(*rstInput)

	return rstInput, nil
}
