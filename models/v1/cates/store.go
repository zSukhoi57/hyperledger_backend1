package cates

import (
	"hyperledger-k8s-be/models"
	"log"
	"github.com/davecgh/go-spew/spew"
)

//Store ...
func Store(mineralName string) (cate *models.Cate, err error) {
	log.Println("--------==models_v1_cates/store")
	cate, err = models.NewCate(mineralName)

	if err != nil {
		return
	}
	spew.Dump(*cate)
	mockCates = append(mockCates, *cate)
	return
}