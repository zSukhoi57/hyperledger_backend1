package cates

import (
	"errors"
	"log"
)

//Destroy ...
func Destroy(id string) (error) {
	log.Println("--------==models_v1_cates/destroy, id:", id)
	var exists bool

	for index, ztype1 := range mockCates {
		if ztype1.ID == id {
			mockCates = append(mockCates[:index], mockCates[index+1:]...)
			exists = true
		}
	}

	if !exists {
		return errors.New("unable to delete ztype1 because ztype1 was not found")
	}

	return nil
}