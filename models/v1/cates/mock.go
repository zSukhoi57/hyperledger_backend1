package cates

import (
	"log"
	"hyperledger-k8s-be/models"
)

var mockCates models.Cates

func init() {
	log.Printf("--------==models_v1_cates/mock > init()")
	iron, _ := models.NewCate("Iron")
	copper, _ := models.NewCate("Copper")
	platinum, _ := models.NewCate("Platinum")

	mockCates = models.Cates{
		*iron, 
		*copper, 
		*platinum, 
	}
}