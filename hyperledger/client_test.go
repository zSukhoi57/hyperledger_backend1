package hyperledger

// run this inside: NickKotenberg_modified/s5-connecting-everythingWk/back-end-working-v2/hyperledger
//$ go test
// should see the query results as a successful test!!!

import (
	"fmt"
	"testing"
)

func Test_ConnectionTest_Success(t *testing.T) {
	fmt.Println("---------------==client_test() check1")

	// fix your own config.yaml path!
	clients := NewClientMap(
		"test-network",
		"/mnt/sda5/0Programming/HyperLedger/NickKotenberg_modified/s5-connecting-everythingWk/back-end-working-v2/hyperledger/config.yaml",
	) // Notice "," at the above end
	// use cat path_to_file to check the path!

	fmt.Println("---------------==client_test() check2")
	_, err := clients.AddClient(
		"Admin",
		"org1",
		"mainchannel",
	)
	if err != nil {
		t.Fatal(err)
		return
	}

	fmt.Println("---------------==client_test() check3")
	res, err := clients.Query("org1", "items", "index", [][]byte{
		[]byte(""),
		[]byte(""),
	})
	if err != nil {
		t.Fatal(err)
		return
	}
	fmt.Println("---------------==client_test() check4")

	fmt.Println(string(res))
}
