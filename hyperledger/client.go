package hyperledger

//L65 Building API client and querying data

import (
	"fmt"

	"github.com/pkg/errors"

	sdkchannel "github.com/hyperledger/fabric-sdk-go/pkg/client/channel"
	mspclient "github.com/hyperledger/fabric-sdk-go/pkg/client/msp"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/resmgmt"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/errors/retry"
	"github.com/hyperledger/fabric-sdk-go/pkg/core/config"
	"github.com/hyperledger/fabric-sdk-go/pkg/fabsdk"
)

/*
go get -u

https://colobu.com/2020/04/09/accidents-of-etcd-and-go-module/

go get: github.com/coreos/bbolt@v1.3.2 updating to
	github.com/coreos/bbolt@v1.3.4: parsing go.mod:
	module declares its path as: go.etcd.io/bbolt
  but was required as: github.com/coreos/bbolt

replace github.com/coreos/bbolt v1.3.5 => go.etcd.io/bbolt v1.3.5
//replace github.com/coreos/bbolt => go.etcd.io/bbolt v1.3.3

replace go.etcd.io/bbolt v1.3.4 => github.com/coreos/bbolt v1.3.4

*/
type orgClient struct {
	User          string
	Org           string
	ChannelClient *sdkchannel.Client
}

type clientList map[string]*orgClient

// Clients ...
type Clients struct {
	Name       string
	ConfigPath string
	List       clientList
}

/*AddClient ...
_, err := clients.AddClient(
	"Admin",
	"org1",
	"mainchannel",
)
*/
func (c *Clients) AddClient(user string, org string, channel string) (*orgClient, error) {
	fmt.Println("... client > AddClient() check1")
	if val, ok := c.List[org]; ok {
		return val, errors.New("client already exists")
	}

	fmt.Println("... client > AddClient() check2")
	sdk, err := fabsdk.New(config.FromFile(c.ConfigPath))
	if err != nil {
		return nil, errors.WithMessage(err, "unable to get config from file")
	}

	fmt.Println("... client > AddClient() check3")
	resourceManagerClientContext := sdk.Context(
		fabsdk.WithUser(user),
		fabsdk.WithOrg(org),
	)

	// resmgmt is resource management
	if _, err := resmgmt.New(resourceManagerClientContext); err != nil {
		return nil, errors.WithMessage(err, "unable to get user resource management")
	}

	fmt.Println("... client > AddClient() check4")
	mspclient, err := mspclient.New(
		sdk.Context(),
		mspclient.WithOrg(org),
	)
	if err != nil {
		return nil, errors.WithMessage(err, "unable to get msp client")
	}

	fmt.Println("... client > AddClient() check5")
	if _, err = mspclient.GetSigningIdentity(user); err != nil {
		return nil, errors.WithMessage(err, "unable to get signing identity")
	}

	fmt.Println("... client > AddClient() check6")
	clientContext := sdk.ChannelContext(
		channel,
		fabsdk.WithUser(user),
		fabsdk.WithOrg(org),
	)

	fmt.Println("... client > AddClient() check7\n", clientContext)
	channelClient, err := sdkchannel.New(clientContext)
	if err != nil {
		return nil, errors.WithMessage(err, "unable to make client from channel context")
	}

	fmt.Println("... client > AddClient() check8")
	c.List[org] = &orgClient{
		User:          user,
		Org:           org,
		ChannelClient: channelClient,
	}

	fmt.Println("... client > AddClient() check9 end")
	return c.List[org], nil
}

// GetClient ...
func (c *Clients) GetClient(org string) (*orgClient, error) {
	if val, ok := c.List[org]; ok {
		return val, nil
	}
	return nil, errors.New("no client for org " + org)
}

/*Query ...
res, err := clients.Query("org1", "items", "index", [][]byte{
	[]byte(""),
	[]byte(""),
})
*/
func (c *Clients) Query(org string, chaincode string, fcn string, args [][]byte) (ret []byte, err error) {
	client, err := c.GetClient(org)
	if err != nil {
		return
	}

	resp, err := client.ChannelClient.Query(
		sdkchannel.Request{
			ChaincodeID: chaincode,
			Fcn:         fcn,
			Args:        args,
		},
		sdkchannel.WithTargetEndpoints("peer0-"+org+"-service"),
		sdkchannel.WithRetry(retry.DefaultChannelOpts),
	) // Fcn: function

	ret = resp.Payload

	return
}

// Invoke ... the same as query plus transient data map
func (c *Clients) Invoke(org string, chaincode string, fcn string, args [][]byte) (ret []byte, err error) {
	client, err := c.GetClient(org)
	if err != nil {
		return
	}

	// transientDataMap
	transientDataMap := make(map[string][]byte)
	transientDataMap["result"] = []byte("Transient Data in invoke func")

	// notiece "Execute" in contrast to Query
	resp, err := client.ChannelClient.Execute(
		sdkchannel.Request{
			ChaincodeID:  chaincode,
			Fcn:          fcn,
			Args:         args,
			TransientMap: transientDataMap,
		},
		sdkchannel.WithTargetEndpoints("peer0-"+org+"-service"),
		sdkchannel.WithRetry(retry.DefaultChannelOpts),
	)

	ret = resp.Payload

	return
}

// NewClientMap ...
func NewClientMap(name string, configPath string) *Clients {
	clients := new(Clients)

	clients.Name = name
	clients.ConfigPath = configPath
	clients.List = make(clientList)

	return clients
}
