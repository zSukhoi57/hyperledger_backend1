package server
/*-----------------------------==
main.go
> ./server/interface.go
	> ./server/server: Init()
		> router > GetRouter()
			> routeer/routes.go:GetRoutes()
	> Start() > s.HTTPServer.ListenAndServer()

to add routes:
add routes inside router/routes.go:GetRoutes()
add routes file inside routes/xyz/xyz.go


requests inbound -> routes
> home.go   > Index()
> status.go > Index()
*/
import (
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
	"github.com/gorilla/handlers"
	"hyperledger-k8s-be/router"
)

//Server struct ...
type Server struct {
	Initialized bool
	Addr string
	ReadTimeout time.Duration
	WriteTimeout time.Duration
	MaxHeaderBytes int
	Port int
	HTTPServer *http.Server
}

//Init ...
func (s *Server) Init(port int) error {
	log.Printf("server > Init() check1")
	s.Addr = ":"+ strconv.Itoa(port)
	s.Port = port
	s.ReadTimeout = 10 * time.Second
	s.WriteTimeout = 10 * time.Second
	s.MaxHeaderBytes = 1 << 20 // a really big number

	r := router.GetRouter() //goes to router > GetRouter()

	log.Printf("server > Init() check2")
	//handler1 := handlers.LoggingHandler()(r.GetRawRouter())
	// http://www.gorillatoolkit.org/pkg/handlers
	handler := handlers.LoggingHandler(
		os.Stdout, handlers.CORS(
			handlers.AllowedOrigins([]string{"*"}),
			handlers.AllowedMethods([]string{"GET","PUT","PATCH","POST","DELETE"}),
			handlers.AllowedHeaders([]string{"Content-Type","Origin","Cache-Control","X-App-Token"}),
			handlers.ExposedHeaders([]string{""}),
			handlers.MaxAge(1000),
			handlers.AllowCredentials(),
	)(r.GetRawRouter()))
	//maxAge determines the max age in seconds between preflight requests
	// setup handler CORS -> running the return function to pass into the router, and pass that as the 2nd argument to login handler

	log.Printf("server > Init() check3")
	handler = handlers.RecoveryHandler(handlers.PrintRecoveryStack(true))(handler)
	//to print error to the console

	//make the server
	s.HTTPServer = &http.Server{
		Addr: s.Addr,
		Handler: handler,
		ReadHeaderTimeout: s.ReadTimeout,
		WriteTimeout: s.WriteTimeout,
		MaxHeaderBytes: s.MaxHeaderBytes,
	}
	s.Initialized = true
	log.Printf("server > Init() check4 end")
	return nil
}

//Start the server
func (s *Server) Start() {
	log.Printf("server > Start()")
	if !s.Initialized {
		panic("A server must be initialized before it can be started!")
	}
	log.Printf("Server has started on port: %d", s.Port)
	log.Fatal(s.HTTPServer.ListenAndServe())
}