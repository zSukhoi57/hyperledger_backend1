package server
// interface for testing server/server.go
import (
	"log"
)

//Service ...
type Service interface {
	Init(port int) error
	Start()
}

//NewServer ...
func NewServer() Service {
	log.Printf("\n-----------------== serverNew")
	return &Server{}
}