package router

import (
	"log"
	"hyperledger-k8s-be/models"
	HomeHandler "hyperledger-k8s-be/routes/home"
	StatusHandler "hyperledger-k8s-be/routes/status"
)//to import models.Routes from models/router.go

//GetBasicRoutes ... for all routes!
func GetBasicRoutes() models.Routes {
	// Routes comes from mv1Router: type Routes []Route

	log.Printf("routesBasic > GetRoutes()")
	return models.Routes{//initialize []Routes
		models.Route{
			Name: "Home", Method: "GET", Pattern: "/",
			HandlerFunc: HomeHandler.Index,
		},
		models.Route{
			Name: "Status", Method: "GET", Pattern: "/status",
			HandlerFunc: StatusHandler.Index,
		},//END with "," !!!
	}
}
/*
curl localhost:3000/
curl localhost:3000/status

*/