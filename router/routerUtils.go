package router
// package name the same as our folder
// router/router.go

import (
	"log"
	"hyperledger-k8s-be/models"
	"github.com/gorilla/mux"
)
// http://www.gorillatoolkit.org/pkg/mux

//Router ...
type Router struct {
	RawRouter *mux.Router
}

//GetRawRouter ...
func (r Router) GetRawRouter() *mux.Router {
	return r.RawRouter
}

//AttachSubRouterWithMiddleware ... called from router
func (r *Router) AttachSubRouterWithMiddleware(path string, subroutes models.Routes, middleware mux.MiddlewareFunc) *mux.Router {

	log.Printf("")
	log.Printf("routerUtils > AttachSubRouterWithMiddleware() check1")
	SubRouter := r.RawRouter.PathPrefix(path).Subrouter()
	SubRouter.Use(middleware)//path is v1

	for _, route := range subroutes {
		SubRouter.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(route.HandlerFunc)
	}
	log.Printf("routerUtils > AttachSubRouterWithMiddleware() check2")
	return SubRouter
}

/*
r := mux.NewRouter()
r.HandleFunc("/products/{key}", ProductHandler)
r.HandleFunc("/articles/{cate}/", ArticlesCateHandler)
r.HandleFunc("/articles/{cate}/{id:[0-9]+}", ArticleHandler)

*/