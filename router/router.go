package router

// interface for testing the router/router.go

import (
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"

	"hyperledger-k8s-be/hyperledger"
	v1routes "hyperledger-k8s-be/routes/v1"
	//	ItemsModel "hyperledger-k8s-be/models/v1/items"
	//"hyperledger-k8s-be/models"
	//"github.com/davecgh/go-spew/spew"
)

const (
	staticDir = "/static/"
)

//Service ... list the function interface
type Service interface {
	GetRawRouter() *mux.Router
}

// GetRouter ...
func GetRouter() Service {
	log.Printf("router > GetRouter() check1")
	r := Router{
		RawRouter: mux.NewRouter().StrictSlash(true),
	} // StrictSlash makes /users/ and /users the same routes! read doc for post/put different responses! package https://github.com/gorilla/mux
	// http://www.gorillatoolkit.org/pkg/mux#Router.StrictSlash

	configPath := os.Getenv("HYPERLEDGER_CONFIG_PATH")
	// export HYPERLEDGER_CONFIG_PATH=/mnt/sda5/0Programming/HyperLedger/NickKotenberg_modified/s5-connecting-everythingWk/back-end-working-v2/hyperledger/config.yaml

	if len(configPath) == 0 {
		panic("ENV var 'HYPERLEDGER_CONFIG_PATH' is not set. Unable to connect to network")
	}
	log.Printf("configPath=" + configPath+"==")
	//"/mnt/sda5/0Programming/HyperLedger/NickKotenberg_modified/s5-connecting-everythingWk/back-end-working-v2/dockerHL211/crypto-config.yaml"

	clients := hyperledger.NewClientMap(
		"test-network", configPath,
	) // Add ',' at the end of argument list!!!

	mode := 1
	var isOnlyAPI bool
	if mode == 1 {
		_ = clients
		isOnlyAPI = true
	} else {
		_, err := clients.AddClient(
			"Admin",
			"org1",
			"mainchannel",
		)
		if err != nil {
			panic(err)
		}
	}
	log.Printf("router > GetRouter() check2")

	//Static File server: to send a static file to client without restarting the server!
	r.RawRouter.
		PathPrefix(staticDir).
		Handler(http.StripPrefix(
			staticDir,
			http.FileServer(http.Dir("."+staticDir))),
		) //curl localhost:3000/static/test.txt

	log.Printf("router > GetRouter() check3")
	// attach basic routes to our router
	// routesBasic > GetBasicRoutes()
	for _, route := range GetBasicRoutes() {
		r.RawRouter.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(route.HandlerFunc)
	} //curl localhost:3000

	log.Printf("router > GetRouter() check4")
	for name, pack := range v1routes.GetRoutes(clients, isOnlyAPI) {
		r.AttachSubRouterWithMiddleware(name, pack.Routes, pack.Middleware)
	} /*name = v1, v2, ...,
	pack ... from mv1Router:
		type SubRoutePackage struct {
			Routes Routes
			Middleware func(next http.Handler) http.Handler
		}
	*/
	log.Printf("router > GetRouter() check5")
	//items := &mockItems
	// items, err := ItemsModel.Index(clients, isOnlyAPI)
	// if err != nil {
	// 	log.Println(err)
	// }
	//spew.Dump(*items)

	log.Printf("")
	log.Printf("router > GetRouter() check6 end")
	return r
}
