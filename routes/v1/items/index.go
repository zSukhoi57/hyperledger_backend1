package items

import (
	"log"
	"encoding/json"
	"net/http"
	ItemsModel "hyperledger-k8s-be/models/v1/items"
	"hyperledger-k8s-be/hyperledger"

)

//Index ...
func Index(clients *hyperledger.Clients, isOnlyAPI bool) http.HandlerFunc {
	log.Println("")
	log.Println("routes_v1_items/index")
	return func(w http.ResponseWriter, r *http.Request){
		log.Println("--------==routes_v1_items/Index")
		items, err := ItemsModel.Index(clients, isOnlyAPI)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		packet, err := json.Marshal(items)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		w.Write(packet)
	}
}
