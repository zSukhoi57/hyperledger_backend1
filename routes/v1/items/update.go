package items

import (
	"encoding/json"
	"log"
	"net/http"
	"github.com/gorilla/mux"
	"hyperledger-k8s-be/models"
	ItemsModel "hyperledger-k8s-be/models/v1/items"
	"hyperledger-k8s-be/hyperledger"
)


//Update ...
func Update(clients *hyperledger.Clients, isOnlyAPI bool) http.HandlerFunc {
	log.Println("routes_v1_items/update")
	return func(w http.ResponseWriter, r *http.Request) {

		vars := mux.Vars(r)
		id := vars["id"]
		log.Println("--------==routes_v1_items/update. id:", id)
		// id, err := strconv.Atoi(vars["id"])
		// if err != nil {
		// 	http.Error(w, "no Id was found in the URL", http.StatusBadRequest)
		// }

		var opts ItemsModel.UpdateOpts
		var item models.Item

		//grab the item from the body
		decoder := json.NewDecoder(r.Body)
		defer r.Body.Close()
		if err := decoder.Decode(&item);	err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		} 

		if r.Method == "PUT" {
			opts.Replace = true
			log.Println("set opts.Replace = true")
		}
		//Update item using the model by calling models/v1/update()
		updatedItem, err := ItemsModel.Update(clients, id, &item, &opts, isOnlyAPI) 
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		log.Println("--------==routes_v1_items/update. updatedItem:", updatedItem)

		//marshal to get the packet
		packet, err := json.Marshal(updatedItem)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Write(packet)
	}
}