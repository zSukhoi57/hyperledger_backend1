package items

/*for cates folder
users => cates
user => cate
User => Item
UsersModel = catesModel

for items folder
cates => remove type
*/

import (
	"hyperledger-k8s-be/hyperledger"
	ItemsModel "hyperledger-k8s-be/models/v1/items"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

//Destroy ...
func Destroy(clients *hyperledger.Clients, isOnlyAPI bool) http.HandlerFunc {
	log.Println("routes_v1_items/destroy")
	return func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		id := vars["id"]
		log.Println("--------==routes_v1_items/destroy, id:", id)
		// id, err := strconv.Atoi(vars["id"])
		// if err != nil {
		// 	http.Error(w, "no Id was found in the URL", http.StatusBadRequest)
		// }

		//Destroy item using the model by calling models/v1/Destroy()
		if err := ItemsModel.Destroy(clients, id, isOnlyAPI); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Write([]byte("item has been successfully destroyed"))
	}
}
