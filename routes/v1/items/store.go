package items

import (
	"encoding/json"
	"hyperledger-k8s-be/models"
	ItemsModel "hyperledger-k8s-be/models/v1/items"
	"log"
	"net/http"

	"github.com/davecgh/go-spew/spew"
	"hyperledger-k8s-be/hyperledger"
)


//Store ...
func Store(clients *hyperledger.Clients, isOnlyAPI bool) http.HandlerFunc {
	log.Println("routes_v1_items/store")
	return func(w http.ResponseWriter, r *http.Request) {
		log.Println("--------==routes_v1_items/store ...")
		var item models.Item
		//grab the item from the body
		decoder := json.NewDecoder(r.Body)
		defer r.Body.Close()
		err := decoder.Decode(&item)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		} 

		//store item using the model
		newItem, err := ItemsModel.Store(
			clients, 
			item.Name,
			item.CateID,
			item.Weight,
			//item.ArrivalTime,
			isOnlyAPI,
			) 
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		log.Println("--------==routes_v1_items/store ... \nnewItem:")
		spew.Dump(newItem)

		//marshal to get the packet
		packet, err := json.Marshal(newItem)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Write(packet)
	}
}