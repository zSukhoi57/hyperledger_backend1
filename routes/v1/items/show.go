package items

import (
	"encoding/json"
	ItemsModel "hyperledger-k8s-be/models/v1/items"
	"log"
	"net/http"

	"github.com/davecgh/go-spew/spew"
	"github.com/gorilla/mux"
	"hyperledger-k8s-be/hyperledger"
)

//assume in the stub code for updates, the item exists on the list of mock data


//Show ... from models/v1/items/show.go
// func Show(id string) (*models.Item, error) {
// 	log.Println("models/v1/items/show.go Show()")
// 	for index, item := range mockItems {
// 		if item.ID == id {
// 			return &mockItems[index], nil
// 		}
// 	}

// 	return nil, errors.New("unable to find item")
// }


//Show ...
func Show(clients *hyperledger.Clients, isOnlyAPI bool) http.HandlerFunc {
	log.Println("routes_v1_items/show")
	return func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		id := vars["id"]
		log.Println("--------==routes_v1_items/show, id:", id)
		// id, err := strconv.Atoi(vars["id"])
		// if err != nil {
		// 	http.Error(w, "no Id was found in the URL", http.StatusBadRequest)
		// }

		//Show item using the model by calling models/v1/Show() 
		item, err := ItemsModel.Show(clients, id, isOnlyAPI)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		log.Println("item:")
		spew.Dump(item)

		packet, err := json.Marshal(item)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Write(packet)
	}
}