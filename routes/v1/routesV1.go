package v1
// routes/v1/routesV1.go

import (
	"log"
	"net/http"
	"hyperledger-k8s-be/hyperledger"
	"hyperledger-k8s-be/models"
	ItemHandler "hyperledger-k8s-be/routes/v1/items"
	CateHandler "hyperledger-k8s-be/routes/v1/cates"
	UserHandler "hyperledger-k8s-be/routes/v1/users"
) // github.com/cryptoman7759/hyperledger-k8s-be/routes/v1/users

/*Middleware ... returns a function, so we can pass more arguments...
used in router/routerUtils.go:
func (r *Router) AttachSubRouterWithMiddleware(path string, subroutes models.Routes, middleware mux.MiddlewareFunc) *mux.Router {...}
*/
func Middleware() func(http.Handler) http.Handler {
	log.Println("routesV1 > Middleware()")
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			log.Println("-----------------------------==")
			log.Println("--------==routesV1 > Middleware()")
			next.ServeHTTP(w, r)
		})
	}
}

//GetRoutes ...
// see router/router.go
// remove the argument (clients *hyperledger.Clients) to test the APIs
func GetRoutes(clients *hyperledger.Clients, isOnlyAPI bool) map[string]models.SubRoutePackage {
	log.Printf("routesV1 > GetRoutes() ... returning map of SubRoutePackage{Middleware, Routes}")
	return map[string]models.SubRoutePackage{
		"/v1": {
			Middleware: Middleware(),
			Routes: models.Routes{
				// Users
				models.Route{Name: "UsersIndex", Method: "GET", Pattern: "/users", HandlerFunc: UserHandler.Index()},//routes/v1/users/index.go
				models.Route{Name: "UsersStore", Method: "POST", Pattern: "/users", HandlerFunc: UserHandler.Store()},
				models.Route{Name: "UsersReplace", Method: "PUT", Pattern: "/users/{id}", HandlerFunc: UserHandler.Update()},
				models.Route{Name: "UsersUpdate", Method: "PATCH", Pattern: "/users/{id}", HandlerFunc: UserHandler.Update()},
				models.Route{Name: "UsersDestroy", Method: "DELETE", Pattern: "/users/{id}", HandlerFunc: UserHandler.Destroy()},

				// Cates
				models.Route{Name: "CatesIndex", Method: "GET", Pattern: "/cates", HandlerFunc: CateHandler.Index()},
				models.Route{Name: "CatesStore", Method: "POST", Pattern: "/cates", HandlerFunc: CateHandler.Store()},
				models.Route{Name: "CatesReplace", Method: "PUT", Pattern: "/cates/{id}", HandlerFunc: CateHandler.Update()},
				models.Route{Name: "CatesUpdate", Method: "PATCH", Pattern: "/cates/{id}", HandlerFunc: CateHandler.Update()},
				models.Route{Name: "CatesDestroy", Method: "DELETE", Pattern: "/cates/{id}", HandlerFunc: CateHandler.Destroy()},

				// Items
				models.Route{Name: "ItemsIndex", Method: "GET", Pattern: "/items", HandlerFunc: ItemHandler.Index(clients, isOnlyAPI)},
				models.Route{Name: "ItemsStore", Method: "POST", Pattern: "/items", HandlerFunc: ItemHandler.Store(clients, isOnlyAPI)},
				models.Route{Name: "ItemsReplace", Method: "PUT", Pattern: "/items/{id}", HandlerFunc: ItemHandler.Update(clients, isOnlyAPI)},
				models.Route{Name: "ItemsUpdate", Method: "PATCH", Pattern: "/items/{id}", HandlerFunc: ItemHandler.Update(clients, isOnlyAPI)},
				models.Route{Name: "ItemsDestroy", Method: "DELETE", Pattern: "/items/{id}", HandlerFunc: ItemHandler.Destroy(clients, isOnlyAPI)},

				models.Route{Name: "ItemsShow", Method: "GET", Pattern: "/items/{id}", HandlerFunc: ItemHandler.Show(clients, isOnlyAPI)},
			},
		},
	}
}

/*
-------------------------==items
-----==Show All
curl localhost:3000/v1/items
curl localhost:3000/v1/cates


curl -X POST -d '{"name":"IRON", "cate_id":"<one of cate IDs>","weight":42000}' localhost:3000/v1/items

curl localhost:3000/v1/items
... confirm the new added item is included is returned

-----==Show One
curl localhost:3000/v1/items/<id from show all>

-----==Replace
curl -X PUT -d '{"name":"IRON_BETA","cate_id":"1129","weight":4550}'  localhost:3000/v1/items/<id from show all>
... use PUT method... can delete other data!

-----==Update
curl -X PATCH -d '{"name":"IRON_Delta"}'  localhost:3000/v1/items/<id from show all>

-----==Destroy
curl -X DELETE localhost:3000/v1/items/<item id>


-------------------------==users
-----==UsersIndex
curl localhost:3000/v1/users
... data stored at models/v1/users/mock.go

-----==UsersStore
curl -X POST -d '{"first_name":"Robert","last_name":"Downey","email":"Robert.Downey@gmail.com","password":"RobertDowney"}'  localhost:3000/v1/users

=>>> {"id":"692df939-6d8d-11ea-951e-04d4c401a16e","first_name":"Robert","last_name":"Downey","email":"Robert.Downey@gmail.com","password":"$2a$10$McDq91BYTycRsbniKKuC6O71rnRzuSpQW9yrI0pEKmL/lwurksVAm"}%

curl localhost:3000/v1/users
... confirm the new added user is included in the returned

-----==UsersReplace
curl localhost:3000/v1/users

curl -X PUT -d '{"first_name":"Chris2"}'  localhost:3000/v1/users/<id from GET call>
... use PUT method... can delete other data!

-----==UsersUpdate
curl -X PATCH -d '{"last_name":"Edward"}'  localhost:3000/v1/users/<user id>
... use PATCH method... just update the specific part

-----==UserDestroy
curl -X DELETE localhost:3000/v1/users/<user id>


-------------------------==cates
curl localhost:3000/v1/cates

curl -X POST -d '{"id":"<...>"}'  localhost:3000/v1/cates

curl localhost:3000/v1/cates
... confirm the new added item is included in the returned

-----==Replace
curl localhost:3000/v1/cates

curl -X POST -d '{"id":"<...>"}' localhost:3000/v1/items
... use PUT method... can delete other data!

-----==Update
curl -X PATCH -d '{"id":"<...>"}'  localhost:3000/v1/cates/<item id>

-----==Destroy
curl -X DELETE localhost:3000/v1/cates/<item id>


Node: in production, never pass the pw hash back!
*/
