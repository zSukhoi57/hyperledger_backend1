package cates

import (
	"encoding/json"
	"hyperledger-k8s-be/models"
	cateModel "hyperledger-k8s-be/models/v1/cates"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)


//Update ...
func Update() http.HandlerFunc {
	log.Println("routes_v1_items/update")
	return func(w http.ResponseWriter, r *http.Request) {

		vars := mux.Vars(r)
		id := vars["id"]
		log.Println("--------==routes_v1_cates/update. id:", id)
		// id, err := strconv.Atoi(vars["id"])
		// if err != nil {
		// 	http.Error(w, "no Id was found in the URL", http.StatusBadRequest)
		// }

		var opts cateModel.UpdateOpts
		var cate models.Cate

		//grab the cate from the body
		decoder := json.NewDecoder(r.Body)
		defer r.Body.Close()
		if err := decoder.Decode(&cate);	err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		} 

		if r.Method == "PUT" {
			opts.Replace = true
		}
		//Update cate using the model by calling models/v1/update()
		updatedCate, err := cateModel.Update(id, &cate, &opts) 
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		log.Println("--------==routes_v1_cates//update. updatedCate:", updatedCate)

		//marshal to get the packet
		packet, err := json.Marshal(updatedCate)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Write(packet)
	}
}