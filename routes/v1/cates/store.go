package cates

import (
	"encoding/json"
	"hyperledger-k8s-be/models"
	cateModel "hyperledger-k8s-be/models/v1/cates"
	"log"
	"net/http"

	"github.com/davecgh/go-spew/spew"
)


//Store ...
func Store() http.HandlerFunc {
	log.Println("routes_v1_cates/store")
	return func(w http.ResponseWriter, r *http.Request) {
		log.Println("--------==routes_v1_cates/store ...")
		var cate models.Item
		//grab the cate from the body
		decoder := json.NewDecoder(r.Body)
		defer r.Body.Close()
		err := decoder.Decode(&cate)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		} 

		//store cate using the model
		newCate, err := cateModel.Store(cate.Name) 
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		log.Println("--------==routes_v1_cates/store ... \nnewCate:")
		spew.Dump(newCate)

		//marshal to get the packet
		packet, err := json.Marshal(newCate)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Write(packet)
	}
}