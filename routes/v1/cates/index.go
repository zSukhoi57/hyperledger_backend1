package cates

import (
	"log"
	"encoding/json"
	"net/http"
	cateModel "hyperledger-k8s-be/models/v1/cates"
)

//Index ...
func Index() http.HandlerFunc {
	log.Println("")
	log.Println("routes_v1_cates/index")
	return func(w http.ResponseWriter, r *http.Request){
		log.Println("--------==routes_v1_cates/index")

		cates, err := cateModel.Index()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		packet, err := json.Marshal(cates)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		w.Write(packet)
	}
}
