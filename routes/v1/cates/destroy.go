package cates
//users => cates
//user => cate
//UsersModel = cateModel

import (
	"net/http"
	"github.com/gorilla/mux"
	"log"
	cateModel "hyperledger-k8s-be/models/v1/cates"
)


//Destroy ...
func Destroy() http.HandlerFunc {
	log.Println("routes_v1_cates/destroy")
	return func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		id := vars["id"]
		log.Println("--------==routes_v1_cates/destroy(), id:", id)
		// id, err := strconv.Atoi(vars["id"])
		// if err != nil {
		// 	http.Error(w, "no Id was found in the URL", http.StatusBadRequest)
		// }

		//Destroy cate using the model by calling models/v1/Destroy() 
		if err := cateModel.Destroy(id); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Write([]byte("cate has been successfully destroyed"))
	}
}