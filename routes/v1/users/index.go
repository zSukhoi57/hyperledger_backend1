package users

import (
	"log"
	"encoding/json"
	"net/http"
	UsersModel "hyperledger-k8s-be/models/v1/users"
)

//Index ...
func Index() http.HandlerFunc {
	log.Println("")
	log.Printf("routes_v1_users/index")
	return func(w http.ResponseWriter, r *http.Request){
		log.Println("--------==routes_v1_users/index")

		users, err := UsersModel.Index()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		packet, err := json.Marshal(users)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		w.Write(packet)
	}
}
