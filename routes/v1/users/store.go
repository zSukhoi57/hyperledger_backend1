package users

import (
	"log"
	"net/http"
	"encoding/json"
	"hyperledger-k8s-be/models"
	UsersModel "hyperledger-k8s-be/models/v1/users"
	"github.com/davecgh/go-spew/spew"
)


//Store ...
func Store() http.HandlerFunc {
	log.Printf("routes_v1_users/store")
	return func(w http.ResponseWriter, r *http.Request) {
		log.Println("--------==routes_v1_users/store")

		var user models.User
		//grab the user from the body
		decoder := json.NewDecoder(r.Body)
		defer r.Body.Close()
		err := decoder.Decode(&user)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		} 

		//store user using the model
		newUser, err := UsersModel.Store(user.FirstName, user.LastName, user.Email, user.Password) 
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		log.Println("--------==routes_v1_users/store ...  \nnewUser:")
		spew.Dump(newUser)

		//marshal to get the packet
		packet, err := json.Marshal(newUser)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Write(packet)
	}
}
/*
routes/v1/router.go

func GetRoutes(clients *hyperledger.Clients) map[string]models.SubRoutePackage {
	return map[string]models.SubRoutePackage{
		"/v1": {
			Middleware: Middleware(),
			Routes: models.Routes{
				// Users
				models.Route{Name: "UsersIndex", Method: "GET", Pattern: "/users", HandlerFunc: UserHandler.Index()},
				models.Route{Name: "UsersStore", Method: "POST", Pattern: "/users", HandlerFunc: UserHandler.Store()},

*/