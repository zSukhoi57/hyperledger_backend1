package users

import (
	"encoding/json"
	"net/http"
	"log"
	"github.com/gorilla/mux"
	"hyperledger-k8s-be/models"
	UsersModel "hyperledger-k8s-be/models/v1/users"
)


//Update ...
func Update() http.HandlerFunc {
	log.Printf("routes_v1_users/update")
	return func(w http.ResponseWriter, r *http.Request) {

		vars := mux.Vars(r)
		id := vars["id"]
		log.Println("--------==routes_v1_users/update. id:", id)		// id, err := strconv.Atoi(vars["id"])
		// if err != nil {
		// 	http.Error(w, "no Id was found in the URL", http.StatusBadRequest)
		// }

		var opts UsersModel.UpdateOpts
		var user models.User

		//grab the user from the body
		decoder := json.NewDecoder(r.Body)
		defer r.Body.Close()
		if err := decoder.Decode(&user);	err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		} 

		log.Println("r.Method:", r.Method)
		if r.Method == "PUT" {
			log.Println("set opts.Replace = true")
			opts.Replace = true
		}
		//Update user using the model by calling models/v1/update()
		updatedUser, err := UsersModel.Update(id, &user, &opts) 
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		log.Println("--------==router/v1/users. updatedUser:", updatedUser)	
		//marshal to get the packet
		packet, err := json.Marshal(updatedUser)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Write(packet)
	}
}