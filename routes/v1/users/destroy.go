package users

import (
	"net/http"
	"github.com/gorilla/mux"
	"log"
	UsersModel "hyperledger-k8s-be/models/v1/users"
)


//Destroy ...
func Destroy() http.HandlerFunc {
	log.Printf("routes_v1_users/destroy")
	return func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		id := vars["id"]
		log.Println("--------==routes_v1_users/destroy, id:", id)
		// id, err := strconv.Atoi(vars["id"])
		// if err != nil {
		// 	http.Error(w, "no Id was found in the URL", http.StatusBadRequest)
		// }

		//Destroy user using the model by calling models/v1/Destroy() 
		if err := UsersModel.Destroy(id); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Write([]byte("user has been successfully destroyed"))
	}
}