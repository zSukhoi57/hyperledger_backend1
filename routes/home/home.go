package home

import "net/http"

//Index ... w is an interface(w is actually a pointer), r is a pointer
func Index(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("/ is working"))
}
/* testing: 
curl localhost:8080
*/