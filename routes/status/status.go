package status

import "net/http"

//Index ... w is an interface(like a pointer), r is a pointer
func Index(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("/status is alive!"))
}
/* testing: 
curl localhost:3000/status
*/